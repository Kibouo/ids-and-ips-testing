Start and application as follows to attach it to the IDS' "company" network. `<ip_addr_for_app>` must be in the IP range specified in [the .env file](../.env).
```sh
sudo docker run --network=<IDS>_company --ip <ip_addr_for_app> <application_name>
```