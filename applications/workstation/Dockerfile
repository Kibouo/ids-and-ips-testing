FROM ubuntu:latest

# suppress interactivity during tzdata installation
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/London"

RUN apt-get update -y
RUN apt-get install -y \
        build-essential \
        pkg-config \
        libssl-dev \
        pciutils \
        curl \
        git

# install WannaMine
RUN cd /opt \
    && git clone https://gitlab.com/Kibouo/wannamine.git
COPY .env /opt/wannamine
RUN cd /opt/wannamine \
    && curl https://sh.rustup.rs -sSf | sh -s -- -y \
    && PATH="$HOME/.cargo/bin:$PATH" rustup override set nightly-2020-03-01 \
    && PATH="$HOME/.cargo/bin:$PATH" PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH cargo build --release

# install WannaCry scanner
RUN mkdir /opt/scanner
COPY ./wanna_scan.cpp /opt/scanner/wanna_scan.cpp
RUN cd /opt/scanner \
    && g++ ./wanna_scan.cpp -o wanna_scan -lpthread

CMD ["sleep", "4h"]
