#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ifaddrs.h>
#include <string>
#include <vector>
#include <math.h>
#include <thread>
#include <mutex>
#include <chrono>

// This script will simulate WannaCry's network scan behaviour.
// That is, it scans the network for open 445 ports with a given amount of threads,
// and sometimes waits several minutes before finishing up because it found a port to
// exploit.
// This is a simulation and thus:
// - generates network traffic based on RE'd WC code
// - does NOT send exploits
// - waiting and scan result are NOT related
// - waiting is based on a random parameter
//
// Original working described: https://www.forcepoint.com/blog/x-labs/wannacry-post-outbreak-analysis

#define SIMULATION_IP "172.28.1.0"
#define SIMULATION_MASK "255.255.255.0"

#define SMB_PORT 445
#define TIMEOUT_SEC 1
#define MAX_NUM_THREADS 10
#define IP_ADDR_NULL_TERM_STRING_LEN 16
#define ETERNALBLUE_WAIT_TIME_SEC 10*60
#define EXPLOITABLE_PORT_RATIO 90.0/100.0

// adapted from https://github.com/svenvdz/wannacry/blob/master/worm.c for Linux
int32_t scan(char const* const ip_addr) {
    int32_t const sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // failed to open socket?
    if (sock_fd < 0) {
        printf("Failed to open socket.\n");
        return 0;
    }

    // set socket to non-blocking
    int32_t flag = 0; // set to 0 to ensure we wait for connection
    ioctl(sock_fd, FIONBIO, &flag);

    // init destination data
    struct sockaddr_in addr;
    memset(&addr, '0', sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SMB_PORT);
    if(inet_pton(AF_INET, ip_addr, &addr.sin_addr) < 1) {
        printf("Failed to convert IP-addr string to struct.\n");
        return 0;
    }

    int32_t const conn_result = connect(sock_fd, (sockaddr *)&addr, 16);
    if (conn_result < 0) {
        printf("Failed to connect to %s. Error code: %i.\n", ip_addr, errno);
        return 0;
    }

    // check whether socket is write-able
    fd_set writefds;
    FD_ZERO(&writefds);
    FD_SET(sock_fd, &writefds);
    struct timeval timeout = {TIMEOUT_SEC, 0};
    int32_t const write_possible = select(sock_fd + 1, NULL, (fd_set *)&writefds, NULL, (timeval *)&timeout);
    if (write_possible < 1) {
        close(sock_fd);
        printf("Failed to get write-permissions to %s.\n", ip_addr);
        return 0;
    }

    // clean up
    close(sock_fd);
    printf("Ping result to %s: %i\n", ip_addr, write_possible);
    return write_possible;
}

unsigned int bin_mask_to_dec(in_addr_t const bin_mask) {
    uint32_t decimal_mask = 0;
    in_addr_t bin_mask_cp = bin_mask;
    while (bin_mask_cp) {
        decimal_mask += bin_mask_cp & 1;
        bin_mask_cp >>= 1;
    }
    return decimal_mask;
}

std::string bin_ip_to_string(in_addr_t const bin_ip) {
    char result[IP_ADDR_NULL_TERM_STRING_LEN];
    inet_ntop(AF_INET, &bin_ip, result, IP_ADDR_NULL_TERM_STRING_LEN);
    return std::string{result};
}

std::vector<std::string> generate_ips(in_addr_t const ip_addr, in_addr_t const netmask, uint32_t const netmask_dec) {
    std::vector<std::string> ips{};

    auto base_subnet_ip = ip_addr & netmask;
    printf(" | base: %s", bin_ip_to_string(base_subnet_ip).c_str());

    uint32_t amt_ips = pow(2, 32 - netmask_dec) - 1;
    printf(" | bcast: %s\n", bin_ip_to_string(ntohl(htonl(ip_addr & netmask) + amt_ips)).c_str());

    for (size_t i = 1; i < amt_ips; i++)
    {
        auto ip = ntohl(htonl(ip_addr & netmask) + i);
        ips.push_back(bin_ip_to_string(ip));
    }

    return ips;
}

/// NOTE: this doesn't work in docker :)))
// does some filtering to get only interesting if's
// std::vector<std::string> get_ips() {
//     struct ifaddrs* ifap_orig;
//     getifaddrs(&ifap_orig);

//     std::vector<std::string> ips = {};
//     struct ifaddrs* ifap = ifap_orig;
//     do {
//         auto const if_name = std::string{ifap->ifa_name};
//         if (if_name == std::string{"lo"}) {
//             ifap = ifap->ifa_next;
//             continue;
//         }

//         in_addr_t const ip_addr = ifap->ifa_addr
//             ? ((sockaddr_in *)(ifap->ifa_addr))->sin_addr.s_addr
//             : 0;
//         auto const ip_addr_str = bin_ip_to_string(ip_addr);
//         if (ip_addr_str == std::string{"0.0.0.0"} || ip_addr_str == std::string{""}) {
//             ifap = ifap->ifa_next;
//             continue;
//         }

//         in_addr_t const netmask = ifap->ifa_netmask
//             ? ((sockaddr_in *)(ifap->ifa_netmask))->sin_addr.s_addr
//             : 0;
//         uint32_t const netmask_dec = bin_mask_to_dec(netmask);
//         /// NOTE: only use big masks to decrease runtime (testing only)
//         if (netmask_dec == 0 || netmask_dec < 24) {
//             ifap = ifap->ifa_next;
//             continue;
//         }

//         printf("%s: %s/%u",
//             if_name.c_str(),
//             ip_addr_str.c_str(),
//             netmask_dec
//         );

//         auto const new_ips = generate_ips();
//         ips.insert(ips.end(), new_ips.begin(), new_ips.end());

//         ifap = ifap->ifa_next;
//     } while (ifap->ifa_next != NULL);

//     freeifaddrs(ifap_orig);
//     return ips;
// }

std::vector<std::string> get_ips() {
    in_addr_t ip_addr_bin;
    if(inet_pton(AF_INET, SIMULATION_IP, &ip_addr_bin) < 1) {
        printf("Failed to convert IP-addr string to binary.\n");
        return {};
    }

    in_addr_t netmask_bin;
    if(inet_pton(AF_INET, SIMULATION_MASK, &netmask_bin) < 1) {
        printf("Failed to convert Mask-addr string to binary.\n");
        return {};
    }

    return generate_ips(ip_addr_bin, netmask_bin, 24);
}

uint32_t amt_running_threads = 0;
std::mutex threads_mutex;

void thread_entry(std::string const ip_addr, bool const is_vulnerable) {
    scan(ip_addr.c_str());

    // simulate wait time for EternalBlue installation
    if (is_vulnerable) {
        printf("'Waiting' for exploit on %s.\n", ip_addr.c_str());
        std::this_thread::sleep_for(std::chrono::seconds(ETERNALBLUE_WAIT_TIME_SEC));
    }

    threads_mutex.lock();
    amt_running_threads -= 1;
    threads_mutex.unlock();
}

int main() {
    srand(0);

    auto const ips = get_ips();
    auto const total_ips = ips.size();
    auto total_ips_seen = 0;

    std::vector<std::thread> thread_handles{};

    while (total_ips_seen != total_ips) {
        threads_mutex.lock();
        while (amt_running_threads < MAX_NUM_THREADS) {
            if (total_ips_seen == total_ips) break;

            thread_handles.push_back(std::thread(
                thread_entry,
                ips[total_ips_seen],
                (rand() % 100) <= (int)(EXPLOITABLE_PORT_RATIO * 100.0)
            ));

            amt_running_threads += 1;
            total_ips_seen += 1;
        }
        threads_mutex.unlock();
    }

    for (auto& handle: thread_handles){
        handle.join();
    }

    return 0;
}