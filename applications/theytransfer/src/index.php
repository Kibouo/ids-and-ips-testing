<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>TheyTransfer</title>
  </head>
  <body class="container">


  <nav class="navbar navbar-light bg-light">
      <div class="container">
          <a class="navbar-brand" href="/">
              <img src="img/theytransfer.svg" alt="">
          </a>
          <span class="navbar-text">
            Send files without bull$h1t
          </span>
      </div>
  </nav>
    <h1>Upload files</h1>
    <?php
        if ((isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK)) {
            $file = $_FILES['file']['tmp_name'];
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $target = 'files/'. basename($file) . '.' . $ext;
            $idx = 0;
            while (file_exists($target)) {
                $target = 'files/' . $idx . basename($file) . '.' . $ext;
                $idx += 1;
            }
            if (move_uploaded_file($_FILES['file']["tmp_name"], $target)) {
                $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/$target"; ?>
                <div class="alert alert-success">
                    Your file is uploaded to <strong><a href="<?= $url ?>"><?= $url ?></a></strong>!
                </div>
<?php       } else { ?>
                <div class="alert alert-danger">
                    An error occurred :( <?= $_FILES['file']['error'] ?>
                </div>
            <?php }


            ?>

<?php   } else { ?>
            <form method="post" enctype="multipart/form-data">
                <div>
                    <label for="formFileLg" class="form-label">Pick a file to upload</label>
                    <input class="form-control form-control-lg" name="file" id="formFileLg" type="file">
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" type="submit">Upload file</button>
                </div>
                <p>We support files up to  <?= min((int)(ini_get('upload_max_filesize')), (int)(ini_get('post_max_size')), (int)(ini_get('memory_limit'))) ?> MB</p>
            </form>
<?php   } ?>
    <script src="js/bootstrap.bundle.min.js"></script>
  </body>
</html>
