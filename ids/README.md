To start an IDS, the `.env` file must be specified:
```sh
sudo docker-compose --env-file <IDS>/.env -f <IDS>/docker-compose.yml up --build
```

Go to [Kibana's Discover tab](http://localhost:5601/app/discover#) to watch traffic logs.