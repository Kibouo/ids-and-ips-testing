##! This script detects the upload, via PHP, of the "well-known" browser shell (located
##! in the `utils` dir)

export {
    redef enum Notice::Type += { PHP_Shell_Upload };

    option php_mime_types = /application\/x-php/ |
                            /application\/x-httpd-php/ |
                            /text\/x-php/ |
                            /text\/plain/;
}

# `file_sniff` takes a few bytes from the start of the file to parse meta data
event file_sniff(f: fa_file, meta: fa_metadata)
{
    if ( ! meta?$mime_type ) return;

    # could check for only incoming files
    if ( php_mime_types in meta$mime_type )
        Files::add_analyzer(f, Files::ANALYZER_MD5);
}

# Called by `Files::ANALYZER_MD5`
event file_hash(f: fa_file, kind: string, hash: string)
{
    # detect `utils/php_browser_shell.php`
    if ( kind == "md5" && hash == "802e08e20ddaf77dcaf0535b9e14e0d5" )
    {
        for ( c_id in f$conns )
        {
            NOTICE([$note=PHP_Shell_Upload,
                    $msg=fmt("%s:%s uploaded a known web shell to %s:%s.", c_id$orig_h, c_id$orig_p, c_id$resp_h, c_id$resp_p),
                    $id=c_id]);
        }
    }
}
